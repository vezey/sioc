﻿namespace SiOC
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Configuration;
    using MappingStrategies;

    public class SiOc
    {
        private readonly Configuration.Configuration configuration = new Configuration.Configuration();

        public IConfiguration Configure()
        {
            return this.configuration;
        }

        public IConfiguration Configure(IMappingStrategy mappingStrategy)
        {
            this.configuration.MappingStrategies.Add(mappingStrategy);
            return this.Configure();
        }

        public T Resolve<T>()
        {
            return (T)this.Resolve(typeof(T));
        }


        private object Resolve(Type type)
        {
            var mappedType = this.MappedType(type);
            return this.Instance(mappedType);
        }

        private object Instance(Type mappedType)
        {
            var constructorInfo = mappedType.GetConstructors().First();
            var parameterInfos = constructorInfo.GetParameters();
            var constructorArguments = this.GetConstructorArguments(parameterInfos);
            return constructorInfo.Invoke(constructorArguments.ToArray());
        }

        private Type MappedType(Type type)
        {
            var mappedType = this.configuration.GetMappedType(type);

            if (mappedType == null)
            {
                throw new InvalidOperationException($"Cannot resolve type {type.Name}");
            }

            return mappedType;
        }

        private List<object> GetConstructorArguments(IEnumerable<ParameterInfo> parameterInfos)
        {
            return parameterInfos.Select(parameterInfo => this.Resolve(parameterInfo.ParameterType)).ToList();
        }
    }
}