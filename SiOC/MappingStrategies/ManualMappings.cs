﻿namespace SiOC.MappingStrategies
{
    using System;
    using System.Collections.Generic;

    internal class ManualMappings : IMappingStrategy
    {
        private readonly Dictionary<Type, Type> dictionary = new Dictionary<Type, Type>();

        public Type GetMappedType(Type type)
        {
            Type result;
            this.dictionary.TryGetValue(type, out result);
            return result;
        }

        public void Add(Type from, Type to)
        {
            this.dictionary.Add(from, to);
        }
    }
}