﻿namespace SiOC.MappingStrategies
{
    using System;

    public interface IMappingStrategy
    {
        Type GetMappedType(Type type);
    }
}