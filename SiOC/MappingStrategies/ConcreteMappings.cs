﻿namespace SiOC.MappingStrategies
{
    using System;

    public class ConcreteMappings : IMappingStrategy
    {
        public Type GetMappedType(Type type)
        {
            return type.IsClass && !type.IsAbstract ? type : null;
        }
    }
}