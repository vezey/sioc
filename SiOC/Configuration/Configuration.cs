﻿namespace SiOC.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using SiOC.MappingStrategies;

    internal class Configuration : IConfiguration
    {
        internal readonly IList<IMappingStrategy> MappingStrategies = new List<IMappingStrategy>();

        private readonly ManualMappings manualMappings = new ManualMappings();

        public Configuration()
        {
            this.MappingStrategies.Add(this.manualMappings);
        }

        public IConfigureFor For<T>()
        {
            return new ConfigureFor(this.manualMappings, typeof(T));
        }

        public Type GetMappedType(Type type)
        {
            return this.MappingStrategies.Select(mappingStrategy => mappingStrategy.GetMappedType(type)).FirstOrDefault(mapping => mapping != null);
        }
    }
}