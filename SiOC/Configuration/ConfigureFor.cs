﻿namespace SiOC.Configuration
{
    using System;

    using SiOC.MappingStrategies;

    internal class ConfigureFor : IConfigureFor
    {
        private readonly ManualMappings manualMappings;

        private readonly Type type;

        public ConfigureFor(ManualMappings manualMappings, Type type)
        {
            this.manualMappings = manualMappings;
            this.type = type;
        }

        public void Use<T>()
        {
            this.manualMappings.Add(this.type, typeof(T));
        }
    }
}