﻿namespace SiOC.Configuration
{
    public interface IConfiguration
    {
        IConfigureFor For<T>();
    }
}