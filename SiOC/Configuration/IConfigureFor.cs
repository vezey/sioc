﻿namespace SiOC.Configuration
{
    public interface IConfigureFor
    {
        void Use<T>();
    }
}