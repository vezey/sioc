﻿namespace Tests
{
    using NUnit.Framework;

    using SiOC;
    using SiOC.MappingStrategies;

    public class ConcreteMappingTests
    {
        [Test]
        public void TestThatConcreteMappingsAreResolved()
        {
            var c = new SiOc();

            c.Configure(new ConcreteMappings());

            var concreteA = c.Resolve<ConcreteA>();
            Assert.That(concreteA, Is.Not.Null);
            Assert.That(concreteA.ConcreteB, Is.Not.Null);
        }

        private class ConcreteA
        {
            public ConcreteA(ConcreteB b)
            {
                this.ConcreteB = b;
            }

            public ConcreteB ConcreteB { get; }
        }

        private class ConcreteB
        {
        }
    }
}
