﻿namespace Tests
{
    using NUnit.Framework;

    using SiOC;

    [TestFixture]
    public class ManualMappingTests
    {
        [Test]
        public void TestThatTryingToResolveAnUnMappedTypeThrowsAnException()
        {
            var container = new SiOc();

            container.Configure().For<IServiceA>().Use<ServiceANoConstructor>();

            Assert.That(() => container.Resolve<IServiceB>(), Throws.InvalidOperationException);
        }

        [Test]
        public void TestThatAServiceWithAParameterlessConstructorCanBeResolved()
        {
            var container = new SiOc();

            container.Configure().For<IServiceA>().Use<ServiceANoConstructor>();

            Assert.That(container.Resolve<IServiceA>(), Is.InstanceOf<ServiceANoConstructor>());
        }

        [Test]
        public void TestThatAServiceWithConstructorParametersCanBeResolved()
        {
            var container = new SiOc();

            container.Configure().For<IServiceA>().Use<ServiceAWithConstructor>();
            container.Configure().For<IServiceB>().Use<ServiceB>();
            container.Configure().For<IServiceC>().Use<ServiceC>();
            container.Configure().For<IServiceD>().Use<ServiceD>();

            var serviceA = container.Resolve<IServiceA>();

            Assert.That(serviceA, Is.InstanceOf<ServiceAWithConstructor>());
            Assert.That(serviceA.ServiceB, Is.InstanceOf<ServiceB>());
            Assert.That(serviceA.ServiceC, Is.InstanceOf<ServiceC>());
            Assert.That(serviceA.ServiceB.ServiceD, Is.InstanceOf<ServiceD>());
        }

        [Test]
        public void TestThatInstancesAreUnique()
        {
            var container = new SiOc();

            container.Configure().For<IServiceA>().Use<ServiceAWithConstructor>();
            container.Configure().For<IServiceB>().Use<ServiceB>();
            container.Configure().For<IServiceC>().Use<ServiceC>();
            container.Configure().For<IServiceD>().Use<ServiceD>();

            var serviceA = container.Resolve<IServiceA>();

            Assert.That(container.Resolve<IServiceB>(), Is.InstanceOf<ServiceB>().And.Not.SameAs(((ServiceAWithConstructor)serviceA).ServiceB));
        }

        private interface IServiceA
        {
            IServiceB ServiceB { get; }

            IServiceC ServiceC { get; }
        }

        private class ServiceAWithConstructor : IServiceA
        {
            public ServiceAWithConstructor(IServiceB serviceB, IServiceC serviceC)
            {
                this.ServiceB = serviceB;
                this.ServiceC = serviceC;
            }

            public IServiceB ServiceB { get; }

            public IServiceC ServiceC { get; }
        }


        private class ServiceANoConstructor : IServiceA
        {
            public IServiceB ServiceB { get; }

            public IServiceC ServiceC { get; }
        }

        private interface IServiceB
        {
            IServiceD ServiceD { get; set; }
        }

        private class ServiceB : IServiceB
        {
            public ServiceB(IServiceD serviceD)
            {
                this.ServiceD = serviceD;
            }

            public IServiceD ServiceD { get; set; }
        }

        private interface IServiceC
        {
        }

        private class ServiceC : IServiceC
        {
        }

        private interface IServiceD
        {
        }

        private class ServiceD : IServiceD
        {
        }
    }
}